<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('inc.head')
    </head>
    <body id="bg-img" style="background: url('project/public/img/bg.jpeg'); 
    background-attachment: fixed; background-size: cover;">
      @include('inc.nav')
      @yield('content')
      @include('inc.JS')
    </body>
</html>
