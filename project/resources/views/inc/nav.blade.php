<header>
        <div class="menu-btn">
            <div class="btn-line"></div>
            <div class="btn-line"></div>
            <div class="btn-line"></div>
        </div>

        <nav class="menu">
            <div class="menu-branding">
            <img src="project/public/img/profile.jpg" alt="Bakibillah Sakib" class="bio-image portrait" style="width:250px !important; height:250px;">

<!-- <div class="portrait" style="background: url('project/public/img/bg.jpeg'); width: 250px;
"> -->
                  
                </div>
            </div>
            <ul class="menu-nav">
                <li class="nav-item current">
                    <a href="{{ URL('/') }}" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL('/about') }}" class="nav-link">About Me</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL('/work') }}" class="nav-link">My Work</a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL('/contact') }}" class="nav-link">How To Reach Me</a>
                </li>
            </ul>
        </nav>   
    </header>