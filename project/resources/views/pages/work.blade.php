@extends('layouts.main')
@section('title', '| View My Work')
@section('content')
    <main id="work">
        <h1 class="lg-heading">My <span class="text-secondary">Work</span></h1>
        <h2 class="sm-heading">
         Check out some of my works..
        </h2>
        <div class="projects">
            <div class="item">
              <a href="http://sakibian.com/travelville/" target="_blank">
                <img src="project/public/img/travelville.png" alt="Project">
              </a>
              <a href="http://sakibian.com/travelville/" class="btn-light" target="_blank">
                <i class="fas fa-eye"></i> Travelville 
              </a>
              <a href="https://github.com/sakibian/travelville-theme" class="btn-dark" target="_blank">
                <i class="fab fa-github"></i> Github
              </a>
            </div>
            <div class="item">
              <a href="http://sakibian.com/acme/index.html" target="_blank">
                <img src="project/public/img/acme.png" alt="Project">
              </a>
              <a href="http://sakibian.com/acme/index.html" class="btn-light" target="_blank">
                <i class="fas fa-eye"></i> Acme HTML5 Theme
              </a>
              <a href="https://github.com/sakibian/HTML5-Responsive-Website" class="btn-dark" target="_blank">
                <i class="fab fa-github"></i> Github
              </a>
            </div>
            <div class="item">
              <a href="http://buddylunch.com/" target="_blank">
                <img src="project/public/img/buddylunch.png" alt="Project">
              </a>
              <a href="http://buddylunch.com/" class="btn-light" target="_blank">
                <i class="fas fa-eye"></i> Buddylunch
              </a>
              <a href="https://bitbucket.org/despro/buddylunch/src/master/" class="btn-dark" target="_blank">
                <i class="fab fa-bitbucket"></i> Bitbucket
              </a>
            </div>
            <div class="item">
              <a href="http://sakibian.com/my-admin/" target="_blank">
                <img src="project/public/img/myadmin.png" alt="Project">
              </a>
              <a href="http://sakibian.com/my-admin/" class="btn-light" target="_blank">
                <i class="fas fa-eye"></i> MyAdmin Theme
              </a>
              <a href="https://github.com/sakibian/my-admin" class="btn-dark" target="_blank">
                <i class="fab fa-github"></i> Github
              </a>
            </div>
            <div class="item">
              <a href="http://sakibian.com/church/" target="_blank">
                <img src="project/public/img/church.png" alt="Project">
              </a>
              <a href="http://sakibian.com/church/" class="btn-light" target="_blank">
                <i class="fas fa-eye"></i> Fellowship Web Portal
              </a>
              <a href="#" class="btn-dark">
                <i class="fab fa-gitlab"></i> GitLab
              </a>
            </div>

            <div class="item">
              <a href="http://sakibian.com/mytunes" target="_blank">
                <img src="project/public/img/mytunes.png" alt="Project">
              </a>
              <a href="http://sakibian.com/mytunes" class="btn-light" target="_blank">
                <i class="fas fa-eye"></i> myTunes Theme
              </a>
              <a href="https://github.com/sakibian/mytunes-theme" class="btn-dark" target="_blank">
                <i class="fab fa-github"></i> Github
              </a>
            </div>
            <div class="item">
              <a href="http://jmtavia.com/" target="_blank">
                <img src="project/public/img/jmtavia.png" alt="Project">
              </a>
              <a href="http://jmtavia.com/" class="btn-light" target="_blank">
                <i class="fas fa-eye"></i> JMT Aviation.
              </a>
              <a href="#" class="btn-dark">
                <i class="fab fa-gitlab"></i> GitLab
              </a>
            </div>
        </div>
    </main>
    @include('inc.footer')
@endsection
