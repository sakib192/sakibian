@extends('layouts.main')
@section('title', '| Welcome')
@section('content')
    <main id="home">
        <h1 class="lg-heading">Bakibillah <span class="text-secondary">Sakib</span></h1>
        <h2 class="sm-heading">
         Full Stack Web Developer, Programmer & Future Entrepreneur.
        </h2>
        <div class="icons">
            <a href="https://twitter.com/sakib192" target="_blank">
                <i class="fab fa-twitter fa-2x"></i>
            </a>
            <a href="https://www.facebook.com/bakibillahsakib" target="_blank">
                <i class="fab fa-facebook fa-2x"></i>
            </a>
            <a href="https://www.linkedin.com/in/sakib192/" target="_blank">
                <i class="fab fa-linkedin fa-2x"></i>
            </a>
            <a href="https://github.com/sakibian" target="_blank">
                <i class="fab fa-github fa-2x"></i>
            </a>
        </div>
    </main>
@endsection
