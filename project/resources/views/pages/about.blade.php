@extends('layouts.main')
@section('title', '| About Me')
@section('content')
    <main id="about">
        <h1 class="lg-heading">About <span class="text-secondary">Me</span></h1>
        <h2 class="sm-heading">
         Let me tell you few things..
        </h2>
        <div class="about-info">
            <img src="project/public/img/profile.jpg" alt="Bakibillah Sakib" class="bio-image" style="width:250px; height:250px;">

            <div class="bio">
                <h3 class="text-secondary">Full Stack Web Developer</h3>
                <p>Hi, I'm very much like to show you my work. so please review my profile with possitive mind. I'm passonate about my work, trying to get more professional in my passion. Everything you see here design & samples are developed by me</p>
            </div>

            <div class="job job-1">
                <h3>Bethel Campus Fellowship</h3>
                <h6>Full-Stack Web Developer</h6>
                <p>I have build a web application which provides various services for Bethel Campus Fellowship Community.</p>
            </div>
            <div class="job job-2">
                <h3>Buddylunch</h3>
                <h6>Full-Stack Developer</h6>
                <p>A web application which provides various services for those who wish to have lunch with friends.</p>
            </div>
            <div class="job job-3">
                <h3>Customization Work</h3>
                <h6>Full-Stack Developer</h6>
                <p>Completed many customization work for various clients also experinced in working with existing project.</p>
            </div>
        </div>
    </main>
    @include('inc.footer')
@endsection
