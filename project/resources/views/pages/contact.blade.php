@extends('layouts.main')
@section('title', '| Contact Me')
@section('content')
<main id="Contact">
        <h1 class="lg-heading">Contact <span class="text-secondary">Me</span></h1>
        <h2 class="sm-heading">
         this is how you can reach me..
        </h2>
        <div class="boxes">
          <div>
            <span class="text-secondary">Skype:</span>
            sakib4ever
          </div>
          <div>
            <span class="text-secondary">Email:</span>
            developer@sakibian.com 
          </div>
          <div>
            <span class="text-secondary">Phone:</span>
            +880 1715 625999
          </div>
        </div>
    </main>
    @include('inc.footer')
@endsection
